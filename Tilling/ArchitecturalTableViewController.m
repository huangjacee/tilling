//
//  ArchitecturalTableViewController.m
//  Tilling
//
//  Created by Sharp Agency on 28/11/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ArchitecturalTableViewController.h"

@implementation ArchitecturalTableViewController {
    NSIndexPath *selectedCellIndexPath;
    NSArray *cellBackgroundImageArray;
    NSArray *cellTitles;
    NSArray *cellDetailText;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.724 green:0.230 blue:0.066 alpha:0.800];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    cellTitles = @[@"Architectural", @"Products", @"Product Profiles"];
    
    cellBackgroundImageArray = @[[UIImage imageNamed:@"arch_1"], [UIImage imageNamed:@"arch_2"], [UIImage imageNamed:@"arch_7"]];
    
    cellDetailText = @[@"Tilling produce an extensive range of architecturally designed interior and exterior timber products. Our interior range includes raw and pre-fiinished lining boards and our external range includes Western Red Cedar solid timber products such as external cladding, bevel siding, shingles, shakes and capping.\n\nTilling Pre-finished Lining Boards create an instant impression on interior walls and ceilings. They’re perfectly finished with 4 coats of satin ‘Ultra Violet ‘ cured polyurethane ready to install. The Pre-finished range now comes in three timber species giving you a choice in both colour and texture to complement your interior space.\n\nFor our exterior products we use Western Red Cedar which has one of the longest life spans of any North American Softwood. It produces long lengths of timber with true, straight grain. Light weight, easy to work with, easy to finish, possessing outstanding dimensional stability, cedar is a preferred wood for nearly all purposes where attractive appearance or resistance to weather is important.",
                       
                       @"",
                       @""];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [cellTitles count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // set cell identifier
    static NSString *CellIdentifier = @"Cell";
    
    // implement custom uitableview cell
    ArchitecturalTableViewCell *cell = (ArchitecturalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArchitecturalTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    // Configure the cells...
    cell.backgroundImage.image = [cellBackgroundImageArray objectAtIndex:indexPath.section];
    cell.titleLabel.text = [cellTitles objectAtIndex:indexPath.section];
    cell.detailLabel.text = [cellDetailText objectAtIndex:indexPath.section];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView beginUpdates]; // tell the table you're about to start making changes
    
    // If the index path of the currently expanded cell is the same as the index that
    // has just been tapped set the expanded index to nil so that there aren't any
    // expanded cells, otherwise, set the expanded index to the index that has just
    // been selected.
    
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) selectedCellIndexPath = nil;
    else selectedCellIndexPath = indexPath;
    
    [tableView endUpdates]; // tell the table you're done making your changes

    if (indexPath.section == 1) [self performSegueWithIdentifier:@"productsSegue" sender:nil];
    if (indexPath.section == 2) [self performSegueWithIdentifier:@"productProfileSegue" sender:nil];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Compares the index path for the current cell to the index path stored in the expanded
    // index path variable. If the two match, return a height of 100 points, otherwise return
    // a height of 44 points.
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) {
        if (indexPath.section == 0) return 550; // Expanded height
    }
    
    return 56;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        return 224;
    }
    return 0; // you can have your own choice, of course
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}



@end
