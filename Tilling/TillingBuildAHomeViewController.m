//
//  TillingBuildAHomeViewController.m
//  Tilling
//
//  Created by Beau Young on 14/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "TillingBuildAHomeViewController.h"

@interface TillingBuildAHomeViewController () {
    NSArray *titlesArray;
    NSArray *coloursArray;
}

@end

@implementation TillingBuildAHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.rowHeight = 44;
    self.tableView.allowsMultipleSelection = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blueGrid"]];

	// Do any additional setup after loading the view.
    titlesArray = @[@"Roof Trusses",
               @"Braceboard",
               @"FrameSmart - Pine Wall Framing",
               @"Lintels - Laminated Veneer",
               @"Smart Joist System - Blocking",
               @"Smart LVL15 Laminated Veneer",
               @"Smart Joist System - iJoist",
               @"Bearer Joist System - Laminated Veneer"];
    
    coloursArray = @[[UIColor colorWithRed:0.714 green:0.729 blue:0.800 alpha:1.000],
                     [UIColor colorWithRed:0.667 green:0.686 blue:0.749 alpha:1.000],
                     [UIColor colorWithRed:0.624 green:0.639 blue:0.702 alpha:1.000],
                     [UIColor colorWithRed:0.578 green:0.592 blue:0.650 alpha:1.000],
                     [UIColor colorWithRed:0.534 green:0.546 blue:0.600 alpha:1.000],
                     [UIColor colorWithRed:0.490 green:0.501 blue:0.550 alpha:1.000],
                     [UIColor colorWithRed:0.445 green:0.455 blue:0.500 alpha:1.000],
                     [UIColor colorWithRed:0.400 green:0.409 blue:0.450 alpha:1.000]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [titlesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // set cell identifier
    static NSString *CellIdentifier = @"Cell";
    
    // implement custom uitableviewcell
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.textLabel.text = [titlesArray objectAtIndex:indexPath.row];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20];
    
    UIView *cellBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    cellBackgroundView.backgroundColor = [UIColor colorWithRed:0.080 green:0.309 blue:0.442 alpha:1.000];
    cell.selectedBackgroundView = cellBackgroundView;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // set cell background color
    cell.backgroundColor = [coloursArray objectAtIndex:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // show image layers upon cell selection
    if (indexPath.row == 0) self.firstImageView.hidden = NO;
    if (indexPath.row == 1) self.secondImageView.hidden = NO;
    if (indexPath.row == 2) self.thirdImageView.hidden = NO;
    if (indexPath.row == 3) self.fourthImageView.hidden = NO;
    if (indexPath.row == 4) self.fifthImageView.hidden = NO;
    if (indexPath.row == 5) self.sixthImageView.hidden = NO;
    if (indexPath.row == 6) self.seventhImageView.hidden = NO;
    if (indexPath.row == 7) self.eighthImageView.hidden = NO;

}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // hide image layers upon cell deselect
    if (indexPath.row == 0) self.firstImageView.hidden = YES;
    if (indexPath.row == 1) self.secondImageView.hidden = YES;
    if (indexPath.row == 2) self.thirdImageView.hidden = YES;
    if (indexPath.row == 3) self.fourthImageView.hidden = YES;
    if (indexPath.row == 4) self.fifthImageView.hidden = YES;
    if (indexPath.row == 5) self.sixthImageView.hidden = YES;
    if (indexPath.row == 6) self.seventhImageView.hidden = YES;
    if (indexPath.row == 7) self.eighthImageView.hidden = YES;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

@end
