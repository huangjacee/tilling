//
//  ViewController.h
//  Tilling
//
//  Created by Maris on 2/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@property (strong, nonatomic) NSArray *tableItems;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
