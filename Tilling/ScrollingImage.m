//
//  3DScrolling.m
//  Tilling Building Calculator
//
//  Created by Maris on 30/07/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ScrollingImage.h"

@implementation ScrollingImage

@synthesize dir = _dir, currentFrame = _currentFrame, totalFrames = _totalFrames;

- (id)initWithImage:(UIImage *)image{
    
    self = [super initWithImage:image];
    if(self){
        self.userInteractionEnabled = true;
        self.currentFrame = 1;
        self.totalFrames = 40;
        
        self.layer.cornerRadius = kCornerRadius;
        self.layer.borderColor = [[self colorWithR:171 G:171 B:171 A:1] CGColor];
        self.layer.borderWidth = 2;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    startPt = [touch locationInView:nil];
    startFrame = self.currentFrame;
    
    [self changeParentScrolling:false];
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint currentPt = [touch locationInView:nil];
    
    
    float distance = currentPt.x - startPt.x;
    
    
    int totalFrameChange = floorf(distance/kChangeDistance);
    
    int changeFrame = startFrame+totalFrameChange;
    if(changeFrame <= 0){
        changeFrame = self.totalFrames+changeFrame;
    }else if(changeFrame > self.totalFrames){
        changeFrame = changeFrame-self.totalFrames;
    }
    if(changeFrame != self.currentFrame){
        self.currentFrame = changeFrame;
        NSString* filename = [self.dir stringByAppendingFormat:@"%d.png", self.currentFrame];
        [self setImage:[UIImage imageNamed:filename]];
    }
    
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self changeParentScrolling:true];
}

#pragma mark - Start/Stop Parent Scrolling
-(void)changeParentScrolling:(bool)scroll{
    
    //stop parent UIScrollView from moving
    UIView* parentView = [self superview];
    if(parentView.class == UIScrollView.class){
        UIScrollView* parent = (UIScrollView*)parentView;
        [parent setScrollEnabled:scroll];
    }
}

#pragma mark - Color Method
- (UIColor *)colorWithR:(CGFloat)red G:(CGFloat)green B:(CGFloat)blue A:(CGFloat)alpha {
    return [UIColor colorWithRed:(red/255.0) green:(green/255.0) blue:(blue/255.0) alpha:alpha];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
