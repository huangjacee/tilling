//
//  StoreLocatorViewController.h
//  Tilling
//
//  Created by Maris on 7/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ViewController.h"

@interface StoreLocatorViewController : ViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end
