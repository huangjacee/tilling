//
//  ArchitecturalTableViewCell.m
//  Tilling
//
//  Created by Sharp Agency on 28/11/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ArchitecturalTableViewCell.h"

@implementation ArchitecturalTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    
        

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)layoutSubviews {
    [super layoutSubviews];
    
    // this is causing issues by creating a black box before the slide down animation begins.
    self.layer.shadowOffset = CGSizeMake(1, 0);
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowRadius = 2;
    self.layer.shadowOpacity = .4;
    
    CGRect shadowFrame = self.layer.bounds;
    CGPathRef shadowPath = [UIBezierPath bezierPathWithRect:shadowFrame].CGPath;
    self.layer.shadowPath = shadowPath;
    
    if (_noShadow) {
        self.layer.shadowColor = [[UIColor clearColor] CGColor];
        self.layer.shadowRadius = 0;
        self.layer.shadowOpacity = 0;
        
        CGRect shadowFrame = self.layer.bounds;
        CGPathRef shadowPath = [UIBezierPath bezierPathWithRect:shadowFrame].CGPath;
        self.layer.shadowPath = shadowPath;
    }
}

@end
