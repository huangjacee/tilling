//
//  TillingTimberTableViewCell.h
//  Tilling
//
//  Created by Maris on 9/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TillingTimberTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *customTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *customDetailLabel;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@end
