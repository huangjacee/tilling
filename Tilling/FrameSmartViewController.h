//
//  FrameSmartViewController.h
//  Tilling
//
//  Created by Maris on 7/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ViewController.h"

@interface FrameSmartViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *firstTextView;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextView *secondTextView;

@property (strong, nonatomic) UIImageView *termite;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
