//
//  CalculatorListTableViewController.m
//  Tilling
//
//  Created by Maris on 15/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "CalculatorListTableViewController.h"
#import "ArchitecturalTableViewCell.h"

@interface CalculatorListTableViewController () {
    NSArray *labelTitlesArray;
    NSArray *backgroundImages;
}

@end

@implementation CalculatorListTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Create array of titles for cells
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.146 green:0.682 blue:0.759 alpha:0.800];
    
    labelTitlesArray = @[@"Floor Joists",
                         @"Bearers",
                         @"Rafters",
                         @"Lintel",
                         @"SmartJoist Web Hole"];
    
    backgroundImages = @[[UIImage imageNamed:@"smart_1"],
                         [UIImage imageNamed:@"smart_2"],
                         [UIImage imageNamed:@"smart_3"],
                         [UIImage imageNamed:@"smart_4"],
                         [UIImage imageNamed:@"smart_5"]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // set cell identifier
    static NSString *CellIdentifier = @"Cell";
    
    // implement custom uitableview cell
    ArchitecturalTableViewCell *cell = (ArchitecturalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArchitecturalTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    // Selected Background Color
    UIView *cellBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    cellBackgroundView.backgroundColor = [UIColor colorWithRed:0.049 green:0.404 blue:0.553 alpha:1.000];
    cell.selectedBackgroundView = cellBackgroundView;
    
    cell.titleLabel.text = [labelTitlesArray objectAtIndex:indexPath.row];
    cell.detailLabel.text = nil;
    cell.backgroundImage.image = [backgroundImages objectAtIndex:indexPath.row];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) [self performSegueWithIdentifier:@"floorJoistSegue" sender:nil];
    if (indexPath.row == 1) [self performSegueWithIdentifier:@"bearersSegue" sender:nil];
    if (indexPath.row == 2) [self performSegueWithIdentifier:@"raftersSegue" sender:nil];
    if (indexPath.row == 3) [self performSegueWithIdentifier:@"lintelSegue" sender:nil];
    if (indexPath.row == 4) [self performSegueWithIdentifier:@"smartJoistSegue" sender:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 56;
}



@end
