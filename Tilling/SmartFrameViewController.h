//
//  SmartFrameViewController.h
//  Tilling
//
//  Created by Maris on 7/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ViewController.h"

@interface SmartFrameViewController : ViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
