//
//  FloorJoistsViewController.m
//  Tilling
//
//  Created by Maris on 15/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "FloorJoistsViewController.h"
#import "ModelWebViewController.h"
#import "FloorJoistCalcViewController.h"

#define SINGLE_SPAN_IMAGE_NAME @"floorjoist_single"
#define CONTINUOUS_SPAN_IMAGE_NAME @"floorjoist_continuous"

@interface FloorJoistsViewController () {
    NSString *modelImageName;
    NSArray *results;
}
@end

@implementation FloorJoistsViewController

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Below I have created separate methods to handle object creation
    // in order to keep ViewDidLoad looking clean.
    // This class also contains methods linked to IB for textField change events
    
    [self addDataToPickerViewArrays];
    [self createPickerViews];
    
    // after above init, set inputviews for textFields to pickerViews
    [self.spanConditionTextField setInputView:_spanConditionPickerView];
    [self.liveLoadPointLoadTextField setInputView:_liveLoadPointLoadPickerView];
    [self.deadLoadTextField setInputView:_deadLoadPickerView];
    [self.joistSpacingTextField setInputView:_joistSpacingPickerView];
    [self.productTextField setInputView:_productPickerView];
    
    // Set accessoryViews
    [self.spanTextField setInputAccessoryView:_accessoryView];
    [self.spanConditionTextField setInputAccessoryView:_accessoryView];
    [self.liveLoadPointLoadTextField setInputAccessoryView:_accessoryView];
    [self.deadLoadTextField setInputAccessoryView:_accessoryView];
    [self.joistSpacingTextField setInputAccessoryView:_accessoryView];
    [self.productTextField setInputAccessoryView:_accessoryView];
    
    // set string defaults
    self.spanInt = 0;
    self.spanConditionString = @"Single Span";
    self.liveLoadPointLoadString = @"1.5kPa/1.8kN";
    self.deadLoadString = @"40";
    self.ceilingAttachedString = @"1";
    self.joistSpacingString = @"450";
    self.productString = @"SmartJoist";
    
    modelImageName = SINGLE_SPAN_IMAGE_NAME;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
// CellForRow not needed when using static cells
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 9;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // check and activate appropriate textfields depending on cell selected
    if (indexPath.row == 0) [self.spanTextField becomeFirstResponder];
    if (indexPath.row == 1) [self.spanConditionTextField becomeFirstResponder];
    if (indexPath.row == 2) [self.liveLoadPointLoadTextField becomeFirstResponder];
    if (indexPath.row == 3) [self.deadLoadTextField becomeFirstResponder];
    if (indexPath.row == 5) [self.joistSpacingTextField becomeFirstResponder];
    if (indexPath.row == 6) [self.productTextField becomeFirstResponder];
    if (indexPath.row == 7) [self calculate];
    if (indexPath.row == 8) { // Show Model Cell
        if ([_spanConditionString isEqualToString:@"Single Span"]) {
            modelImageName = SINGLE_SPAN_IMAGE_NAME;
            [self performSegueWithIdentifier:@"modelSegue" sender:self];
        }
        else if ([_spanConditionString isEqualToString:@"Continuous Span"]) {
            modelImageName = CONTINUOUS_SPAN_IMAGE_NAME;
            [self performSegueWithIdentifier:@"modelSegue" sender:self];
        }
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    // remove all first responders.
    [self.view endEditing:YES];
}

#pragma mark - UIPickerView data source
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == _spanConditionPickerView) return [_spanConditionArray count];
    if (pickerView == _liveLoadPointLoadPickerView) return [_liveLoadPointLoadArray count];
    if (pickerView == _deadLoadPickerView) return [_deadLoadArray count];
    if (pickerView == _joistSpacingPickerView) return [_joistSpacingArray count];
    if (pickerView == _productPickerView) return [_productArray count];
    
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (pickerView == _spanConditionPickerView) return [_spanConditionArray objectAtIndex:row];
    if (pickerView == _liveLoadPointLoadPickerView) return [_liveLoadPointLoadArray objectAtIndex:row];
    if (pickerView == _deadLoadPickerView) return [_deadLoadArray objectAtIndex:row];
    if (pickerView == _joistSpacingPickerView) return [_joistSpacingArray objectAtIndex:row];
    if (pickerView == _productPickerView) return [_productArray objectAtIndex:row];
    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    // set textViews based on selection made in pickerView
    if (pickerView == _spanConditionPickerView) {
        self.spanConditionTextField.text = [_spanConditionArray objectAtIndex:row];
        self.spanConditionString = [_spanConditionArray objectAtIndex:row];
    }
    if (pickerView == _liveLoadPointLoadPickerView) {
        self.liveLoadPointLoadTextField.text = [_liveLoadPointLoadArray objectAtIndex:row];
        self.liveLoadPointLoadString = [_liveLoadPointLoadArray objectAtIndex:row];
    }
    if (pickerView == _deadLoadPickerView) {
        self.deadLoadTextField.text = [NSString stringWithFormat:@"%@kg/m2", [_deadLoadArray objectAtIndex:row]];
        self.deadLoadString = [_deadLoadArray objectAtIndex:row];
    }
    
    if (pickerView == _joistSpacingPickerView) {
        self.joistSpacingTextField.text = [NSString stringWithFormat:@"%@mm", [_joistSpacingArray objectAtIndex:row]];
        self.joistSpacingString = [_joistSpacingArray objectAtIndex:row];
    }
    
    if (pickerView == _productPickerView) {
        self.productTextField.text = [_productArray objectAtIndex:row];
        self.productString = [_productArray objectAtIndex:row];
    }
}

#pragma mark - IB methods for textField changes
- (IBAction)spanEditingChanged:(id)sender {
    self.spanInt = [_spanTextField.text intValue];
}

- (IBAction)ceilingSwtich:(UISwitch *)sender {
    if ([self.ceilingAttachedSwitch isOn]) self.ceilingAttachedString = @"2";
    else self.ceilingAttachedString = @"1";
}

#pragma mark - Init Methods
- (void)createPickerViews {
    // Separate pickerviews for each occasion
    // makes it a little easier to make smaller custom changes and helps with debugging
    
    // Set up accessoryView. Used to dismiss pickerViews
    self.accessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
    [self.accessoryView setItems:@[_doneButton]];
    
    // SpanCondition PickerView
    self.spanConditionPickerView = [[UIPickerView alloc] init];
    self.spanConditionPickerView.delegate = self;
    self.spanConditionPickerView.dataSource = self;
    
    // LiveLoad PickerView
    self.liveLoadPointLoadPickerView = [[UIPickerView alloc] init];
    self.liveLoadPointLoadPickerView.delegate = self;
    self.liveLoadPointLoadPickerView.dataSource = self;
    
    // DeadLoad PickerView
    self.deadLoadPickerView = [[UIPickerView alloc] init];
    self.deadLoadPickerView.delegate = self;
    self.deadLoadPickerView.dataSource = self;
    
    // JoistSpacing PickerView
    self.joistSpacingPickerView = [[UIPickerView alloc] init];
    self.joistSpacingPickerView.delegate = self;
    self.joistSpacingPickerView.dataSource = self;
    
    // Product PickerView
    self.productPickerView = [[UIPickerView alloc] init];
    self.productPickerView.delegate = self;
    self.productPickerView.dataSource = self;
}

- (void)doneButtonPressed {
    [self.view endEditing:YES];
}

- (void)addDataToPickerViewArrays {
    
    self.spanConditionArray = @[@"Single Span",
                                @"Continuous Span"];
    
    
    self.liveLoadPointLoadArray = @[@"1.5kPa/1.8kN",
                                     @"2kPa/1.8kN",
                                     @"3kPa/2.7kN"];
    
    self.deadLoadArray = @[@"40",
                           @"62",
                           @"100",
                           @"150",
                           @"200"];
    
    self.joistSpacingArray = @[@"300",
                               @"450",
                               @"600"];
    
    self.productArray = @[@"SmartJoist",
                          @"SmartLVL15"];
}

#pragma mark - Perform Calculation
- (void)calculate {
    // This Method will filter through a .plist database creating an array at the end depending on user inputs.
    
    // Load data to filter
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"floorJoistsData" ofType:@"plist"];
    NSArray *contentArray = [NSArray arrayWithContentsOfFile:plistPath];

    // set span condition to integer to match with datasource value
    NSString *spanConditionNumber = @"0";
    if ([_spanConditionString isEqualToString:@"Single Span"]) spanConditionNumber = @"1";
    else if ([_spanConditionString isEqualToString:@"Continuous Span"]) spanConditionNumber = @"2";
    
    // set liveload to integer to match with datasource value
    NSString *liveLoadNumber = @"0";
    if ([_liveLoadPointLoadString isEqualToString:@"1.5kPa/1.8kN"]) liveLoadNumber = @"1";
    else if ([_liveLoadPointLoadString isEqualToString:@"2kPa/1.8kN"]) liveLoadNumber = @"2";
    else if ([_liveLoadPointLoadString isEqualToString:@"3kPa/2.7kN"]) liveLoadNumber = @"3";
    
    // set product id to integer to match with datasource value
    NSString *productIDNumber = @"0";
    if ([_productString isEqualToString:@"SmartJoist"]) productIDNumber = @"1";
    else if ([_productString isEqualToString:@"SmartLVL15"]) productIDNumber = @"2";
    
    // 1 Span Condition Filter
    NSPredicate *spanConditionPredicate = [NSPredicate predicateWithFormat:@"SELF.spanConditionID CONTAINS %@", spanConditionNumber];
    NSArray *filteredBySpan = [NSArray arrayWithArray:[contentArray filteredArrayUsingPredicate:spanConditionPredicate]];
    
    // 2 Ceiling Attatched Filter
    NSPredicate *switchPredicate = [NSPredicate predicateWithFormat:@"SELF.ceilingAttachedID CONTAINS %@", _ceilingAttachedString];
    NSArray *filteredBySwitch = [NSArray arrayWithArray:[filteredBySpan filteredArrayUsingPredicate:switchPredicate]];
    
    // 3 Joist Spacing Filter
    NSPredicate *joistSpacingPredicate = [NSPredicate predicateWithFormat:@"SELF.joistSpacing CONTAINS %@", _joistSpacingString];
    NSArray *filteredByJoistSpacing = [NSArray arrayWithArray:[filteredBySwitch filteredArrayUsingPredicate:joistSpacingPredicate]];

    // 4 Live load filter
    NSPredicate *liveLoadPointLoadPredicate = [NSPredicate predicateWithFormat:@"SELF.fllFplID CONTAINS %@", liveLoadNumber];
    NSArray *filteredbyLiveLoad = [NSArray arrayWithArray:[filteredByJoistSpacing filteredArrayUsingPredicate:liveLoadPointLoadPredicate]];
    
    // 5 Dead load filter
    NSPredicate *deadLoadPredicate = [NSPredicate predicateWithFormat:@"SELF.floorDL CONTAINS %@", _deadLoadString];
    NSArray *filteredByDeadLoad = [NSArray arrayWithArray:[filteredbyLiveLoad filteredArrayUsingPredicate:deadLoadPredicate]];
    
    // 6 Product ID filter
    NSPredicate *productPredicate = [NSPredicate predicateWithFormat:@"SELF.productID CONTAINS %@", productIDNumber];
    NSArray *filteredProductID = [NSArray arrayWithArray:[filteredByDeadLoad filteredArrayUsingPredicate:productPredicate]];
    
    // 7 Span Filter
    NSPredicate *spanPredicate = [NSPredicate predicateWithFormat:@"SELF.span.intValue >= %d", _spanInt];
    NSArray *filteredSpan = [NSArray arrayWithArray:[filteredProductID filteredArrayUsingPredicate:spanPredicate]];
    
    results = filteredSpan;
    
    NSLog(@"Raw Count: %lu", (unsigned long)[contentArray count]);
    NSLog(@"Span Filter Count: %lu", (unsigned long)[filteredBySpan count]);
    NSLog(@"Switch Filter Count: %lu", (unsigned long)[filteredBySwitch count]);
    NSLog(@"Joist Spacing Count: %lu", (unsigned long)[filteredByJoistSpacing count]);
    NSLog(@"Live Load Count: %lu", (unsigned long)[filteredbyLiveLoad count]);
    NSLog(@"Dead Load Count: %lu", (unsigned long)[filteredByDeadLoad  count]);
    NSLog(@"Product ID Count: %lu", (unsigned long)[filteredProductID  count]);
    NSLog(@"Span Count: %lu", (unsigned long)[filteredSpan  count]);

    [self performSegueWithIdentifier:@"resultsSegue" sender:self];
}

#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"modelSegue"]) {
        ModelWebViewController *modelView = [segue destinationViewController];
        [modelView setImageName:modelImageName];
    }
    // Get data from .plist, put into array and add to next viewController
    if ([[segue identifier] isEqualToString:@"resultsSegue"]) {
        
        FloorJoistCalcViewController *vc = [segue destinationViewController];
        [vc setContent:results];
    }
}

@end
