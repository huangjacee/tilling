//
//  SmartStructProductsViewController.m
//  Tilling
//
//  Created by Beau Young on 16/12/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "SmartStructProductsViewController.h"
#import "ArchitecturalTableViewCell.h"

@interface SmartStructProductsViewController ()

@end

@implementation SmartStructProductsViewController {
    NSIndexPath *selectedCellIndexPath;
    NSArray *cellTitles;
    NSArray *cellDetailText;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    cellTitles = @[@"Cross Laminated Timber",
                   @"TecBeam",
                   @"TecSlab"
                   ];
    
    
    cellDetailText = @[@"Tilling have recently been appointment as the Australian and New Zealand distributors of KLH cross laminated timber products.\n\nCross-laminated timber (German abbreviation: KLH) is produced from layers of spruce wood that are arranged crosswise on top of each other and glued to each other with a pressing power of 6 N/mm² to form large-sized solid wood elements. The crosswise arrangement of the longitudinal and transverse layers reduces the swelling and shrinkage of the wood in the plane of the panel to an insignificant minimum and considerably increases the static load-carrying capacity and dimensional stability.\n\nIn order to rule out any damage caused by pests, fungi or insects, in compliance with the European Technical Approval, technically dried wood with a wood moisture of 12% (+/-2%) is Gluing takes place using solvent-free and formaldehyde free PUR adhesive which has been tested in accordance with DIN 68141 and other strict criteria of MPA Stuttgart, and approved for the production of load-bearing and nonload-bearing timber components and special constructions in accordance with DIN 1052 and EN 301. The glue is applied automatically over the entire surface with an optimised amount of adhesive. A high-quality level of adhesion is achieved as a result of the high pressing power.\n\nMAXIMUM SIZE\nMaximum length 16.50 m\nMaximum width 2.95 m\nMaximum thickness 0.50 m\nMinimum production lengths 8 m, respectively in 10 cm increments up to the maximum length\nProduced widths 2.40 / 2.50 / 2.72 / 2.95 m On request 2.25 m\n\nSURFACES\nKLH solid wood panels are offered as standard in nonvisible quality, industrial visible quality and domestic visible quality. Special surfaces can be provided on request. For further information, as well as quality details about the respective surfaces, see the following pages and www.klh.at.\n\nCNC CUTTING\nFactory cutting or beaming takes place using state-of-the art CNC technology, the basis for which are the production and cutting plans released by the client or the executing company, respectively. The cutting accuracy is within the range of tolerances in building construction – according to DIN 18203/Part 3 for wall, floor, ceiling and roof panels made of timber materials. On request and with the appropriate equipment, the panels can also be cut by the relevant construction company. Please also pay attention to the cutting tolerances indicated by us at www.klh.at.\n\nASSEMBLY\nThe cut-to-size KLH solid wood elements are delivered to the construction site just before they are needed, and there they are assembled by expert timber construction companies or construction firms using a building crane in the shortest possible construction period. The links created between tradition, well-founded craftsmanship and state-of-the-art timber construction technology enable individual construction with lasting value and a particular focus on the environment and energy consumption.\n\nPRODUCT ADVANTAGES\n• Ecologically sustainable building material\n• Recommended in terms of building biology\n• Positive ecobalance\n• Healthy, comfortable room climate\n• Solid wood construction with lasting value\n• Freedom in architectural implementation\n• Flexible design without a grid pattern\n• Compatible with steel, glass and other materials\n• Excellent static properties\n• Increasing space thanks to slender construction elements\n• Technically approved and CE certified building product\n• Quality controlled production\n• Prefabricated elements with high dimensional accuracy\n• CNC controlled cutting of the elements\n• Delivery directly to the construction site\n• Easy to install\n• Short construction period\n• Dry construction method\n• Buildings are ready for occupancy in a short time\n\nCross Laminated Timber is a new and innovative building material that permits fast and efficient construction of single or multi-residential type buildings. CLT was rst developed in Austria, and within the last 20 years, production has spread to other parts of Europe and into parts of North America. It is used mainly for wall, ceiling and roof construction.\n\nCLT is fabricated by bonding together timber boards with structural adhesives to produce a solid timber panel with each layer of the panel alternating between longitudinal and transverse lamellae. Alternating the grain directions of each layer of timber reduces many of the weaknesses that previous timber products had. Specically:\nThe effects of shrinkage and swelling are vastly reduced. Load may be transferred in more than one direction.\n\nPerformance-wise, Cross Laminated Timber has proved itself to be superior in comparison to other timber products in terms of:\n\nFire resistance - CLT has a considerable re resistance and is complemented by the use of traditional re resistant material, such as fire resistant plasterboard. Building with CLT means that re must travel through large area of solid timber surface with little cavity available for re to burn and travel through.\n\nNoise Insulation - CLT possesses inherent noise absorbing properties\n\nHeat Insulation - CLT walls are able to absorb a large amount of energy before releasing it into the atmosphere on the other side of the wall.",
                       
                       @"Tecbeam Australasia is proud Australian building products innovator, licensor and manufacturer. Its core technology is the TECBEAM® ‘I’ Beam/Joist, a unique, patent protected, innovative light weight steel and timber composite structural beam.\n\nThe composite TECBEAM® ‘I’ Beam/Joist comprises a continuous light gauge galvanized steel web, with press formed stiffening ribs and uniformly spaced service holes, and structural timber flanges of plantation-grown softwood, fixed by nails and/or spikes to each side of the continuous steel web.\n\nTECBEAM® joists demonstrate many superior performance characteristics when compared with other commonly used joists. They have been extensively tested and proven as a structural beam for use in all types of floor and roof construction in over a decade of supplying the competitive Victorian residential and commercial construction markets.\n\nStructural floor and roof framing systems have been developed utilizing the unique properties of TECBEAM joists. Significant savings in the total cost of construction can be made when TECBEAM® joists are specified.\n\nA TECBEAM® designed floor can often replace structural steel beams, this can amount to significant savings in time and cost. Ceiling plaster board and flooring materials can be fixed directly to the joist flanges, and most services can be installed through the web holes, including air conditioning ducts and large waste pipes, eliminating the need for tradesmen to cut holes in the beam web.",
                       
                       @"The affordable Premium lightweight timber framed floor system that feels like a concrete floor.\n\nTECSLAB™ is designed for low, medium and high-density construction. The TECSLAB™ system combines the properties of two exceptional construction products TECBEAM™ and Hebel® PowerFloor™ to provide a superior floor solution achieving quicker build times and significant cost savings.\n\nHebel® is a lightweight steel-reinforced Autoclaved Aerated Concrete (AAC) that has been used in Europe for over 70 years and here in Australia for over 20 years.\n\nTECBEAM™ is a composite structural ‘I’-Beam which has a continuous galvanized steel web and timber flanges resulting in a lightweight beam with structural properties closely resembling those of a steel beam rather than a solid beam.\n\nTECBEAM™ is designed and manufactured in Australia by licensed fabricators and holds worldwide patents for its technology.\n\nA high-performance lightweight flooring systemTECSLAB™ is a high performance lightweight flooring system which provides:\n1) a superior floor solution, with the qualities and feel of a concrete floor.\n2) rigidity of a steel frame at a significantly reduced cost.\n\nTECSLAB™ can be easily installed by on-site carpenters and is unaffected by wet or changing weather during installation. Unlike wet-pour concrete, TECSLAB™ is installed without the need for curing or propping. Supremely comfortable, solid and low noise emission.\n\nThe TECSLAB™ system reduces foot-fall, airborne noise between floors and eliminates squeaking that is often a problem with other joist systems and particle board flooring.\n\nTECSLAB™ boasts superior thermal performance (particularly for suspended floors) and assists in achieving thermal ratings that reduce heating and cooling costs.\n\nProven in the market, Australian designed and made All components of the TECSLAB™ system are 100% manufactured in Australia. Hebel® is manufactured by CSR Building Products Limited and TECBEAM™ is fabricated by licensed Australian fabricators. You can depend and rely on the product quality, technical expertise, warranty and continuity of stock supplies.\n\nWith TECSLAB™ you can trust that everything has been proven and tested and the components are being continually improved with ongoing research and development."
                       ];
    
    
    
}

#pragma mark - TableView delegate and datasource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [cellTitles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // set cell identifier
    static NSString *CellIdentifier = @"Cell";
    
    // implement custom uitableview cell
    ArchitecturalTableViewCell *cell = (ArchitecturalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArchitecturalTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    // Configure the cells...
    cell.titleLabel.text = [cellTitles objectAtIndex:indexPath.row];
    cell.detailLabel.text = [cellDetailText objectAtIndex:indexPath.row];
    cell.contentView.backgroundColor = [UIColor colorWithRed:0.148 green:0.268 blue:0.448 alpha:0.380];
    
    // Clear backgroundColor for transparency
    cell.backgroundColor = [UIColor clearColor];
    
    // remove shadows
    cell.noShadow = YES;
    
    // check if row is odd or even and set color accordingly
    if (indexPath.row % 2) {
        cell.backgroundImage.backgroundColor = [UIColor colorWithRed:0.202 green:0.972 blue:0.488 alpha:0.600];
    }else {
        cell.backgroundImage.backgroundColor = [UIColor colorWithRed:0.168 green:0.759 blue:0.331 alpha:0.600];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView beginUpdates]; // tell the table you're about to start making changes
    
    // If the index path of the currently expanded cell is the same as the index that
    // has just been tapped set the expanded index to nil so that there aren't any
    // expanded cells, otherwise, set the expanded index to the index that has just
    // been selected.
    
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) selectedCellIndexPath = nil;
    else selectedCellIndexPath = indexPath;
    
    [tableView endUpdates]; // tell the table you're done making your changes
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) {
        // Expanded height
        if (indexPath.row == 0) return 2500; // cross laminated timber
        if (indexPath.row == 1) return 720; // TecBeam
        if (indexPath.row == 2) return 1200; // TecSlab
    }
    return 56;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // remove shadows
    [[cell layer] setShadowOpacity:0.0];
    [[cell layer] setShadowRadius:0.0];
    [[cell layer] setShadowColor:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
