//
//  3DScrolling.h
//  Tilling Building Calculator
//
//  Created by Maris on 30/07/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#define kCornerRadius 10
#define kChangeDistance 15

@interface ScrollingImage : UIImageView{
    NSString* _dir;
    int _currentFrame, _totalFrames, startFrame;
    CGPoint startPt;
}

@property (nonatomic, strong) NSString* dir;
@property (nonatomic, assign) int currentFrame, totalFrames;

@end
