//
//  CustomCalculatorCell.h
//  Tilling
//
//  Created by Maris on 7/11/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCalculatorCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *sizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *spanLabel;
@property (weak, nonatomic) IBOutlet UILabel *ebLabel;

@end
