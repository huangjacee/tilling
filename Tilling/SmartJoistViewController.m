//
//  SmartJoistViewController.m
//  Tilling
//
//  Created by Maris on 15/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "SmartJoistViewController.h"
#import "ModelWebViewController.h"

#define SINGLE_SPAN_IMAGE_NAME @"webhole_single"

@interface SmartJoistViewController () {
    NSString *modelImageName;
    NSArray *results;
}

@end

@implementation SmartJoistViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Below I have created separate methods to handle object creation
    // in order to keep ViewDidLoad looking clean.
    // This class also contains methods linked to IB for textField change events
    
    [self addDataToPickerViewArrays];
    [self createPickerViews];
    
    // After above init, set input for textfield to pickerviews
    [self.joistSpacingTextField setInputView:_joistSpacingPickerView];
    [self.holeShapeTextField setInputView:_holeShapePickerView];
    [self.holeDiameterTextField setInputView:_holeDiameterPickerView];
    [self.jointSizeTextField setInputView:_jointSizePickerView];
    [self.deadLoadTextField setInputView:_deadLoadPickerView];
    
    // Set accessoryViews
    [self.spanTextField setInputAccessoryView:_accessoryView];
    [self.joistSpacingTextField setInputAccessoryView:_accessoryView];
    [self.holeShapeTextField setInputAccessoryView:_accessoryView];
    [self.holeDiameterTextField setInputAccessoryView:_accessoryView];
    [self.jointSizeTextField setInputAccessoryView:_accessoryView];
    [self.deadLoadTextField setInputView:_accessoryView];

    // Set string defaults
    self.spanInt = 0;
    self.joistSpacingString = @"300 to 600";
    self.holeShapeString = @"Circular";
    self.holeDiameterString = @"75";
    self.jointSizeString = @"SJ20044";
    self.deadLoadString = @"40kg/m2, 1.5kPa";
    
    modelImageName = @"webhole_single";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 8;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Check and activate appropriate textfields depending on cell selected

    if (indexPath.row == 0) [self.spanTextField becomeFirstResponder];
    if (indexPath.row == 1) [self.joistSpacingTextField becomeFirstResponder];
    if (indexPath.row == 2) [self.holeShapeTextField becomeFirstResponder];
    if (indexPath.row == 3) [self.holeDiameterTextField becomeFirstResponder];
    if (indexPath.row == 4) [self.jointSizeTextField becomeFirstResponder];
    if (indexPath.row == 5) [self.deadLoadTextField becomeFirstResponder];
    if (indexPath.row == 6) [self calculate];
    if (indexPath.row == 7) [self performSegueWithIdentifier:@"modelSegue" sender:nil];
    
    
    NSLog(@"tapped row: %ld", (long)indexPath.row);

}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    // remove all first responders
    [self.view endEditing:YES];
}

#pragma mark - UIPickerView Data Source
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {

    if (pickerView == _joistSpacingPickerView) return [_joistSpacingArray count];
    if (pickerView == _holeShapePickerView) return [_holeShapeArray count];
    if (pickerView == _holeDiameterPickerView) return  [_holeDiameterArray count];
    if (pickerView == _jointSizePickerView) return [_jointSizeArray count];
    if (pickerView == _deadLoadPickerView) return [_deadLoadArray count];
    
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (pickerView == _joistSpacingPickerView) return [_joistSpacingArray objectAtIndex:row];
    if (pickerView == _holeShapePickerView) return [_holeShapeArray objectAtIndex:row];
    if (pickerView == _holeDiameterPickerView) return  [_holeDiameterArray objectAtIndex:row];
    if (pickerView == _jointSizePickerView) return [_jointSizeArray objectAtIndex:row];
    if (pickerView == _deadLoadPickerView) return [_deadLoadArray objectAtIndex:row];

    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    // set textviews based on selection made in pickerView
    if (pickerView == _joistSpacingPickerView) {
        self.joistSpacingTextField.text =  [_joistSpacingArray objectAtIndex:row];
        self.joistSpacingString = [_joistSpacingArray objectAtIndex:row];
    }
    
    if (pickerView == _holeShapePickerView) {
        self.holeShapeTextField.text = [_holeShapeArray objectAtIndex:row];
        self.holeShapeString = [_holeShapeArray objectAtIndex:row];
    }
    
    if (pickerView == _holeDiameterPickerView) {
        self.holeDiameterTextField.text = [NSString stringWithFormat:@"%@mm", [_holeDiameterArray objectAtIndex:row]];
        self.holeDiameterString = [_holeDiameterArray objectAtIndex:row];
    }
    if (pickerView == _jointSizePickerView) {
        self.jointSizeTextField.text = [_jointSizeArray objectAtIndex:row];
        self.jointSizeString = [_jointSizeArray objectAtIndex:row];
    }
    
    if (pickerView == _deadLoadPickerView) {
        self.deadLoadTextField.text = [_deadLoadArray objectAtIndex:row];
        self.deadLoadString = [_deadLoadArray objectAtIndex:row];
    }
}


#pragma mark - IB methods for textField changes
- (IBAction)spanEditingDidChange:(id)sender {
    self.spanInt = [_spanTextField.text intValue];
}


#pragma mark - init methods
- (void)createPickerViews {
    // Separate pickerviews for each occasion
    // makes it a little easier to make smaller custom changes and helps with debugging
    
    // Set up accessoryView. Used to dismiss pickerViews
    self.accessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
    [self.accessoryView setItems:@[_doneButton]];
    
    // JoistSpacing PickerView
    self.joistSpacingPickerView = [[UIPickerView alloc] init];
    self.joistSpacingPickerView.delegate = self;
    self.joistSpacingPickerView.dataSource = self;
    
    // HoleShape PickerView
    self.holeShapePickerView = [[UIPickerView alloc] init];
    self.holeShapePickerView.delegate = self;
    self.holeShapePickerView.dataSource = self;
    
    // HoleDiameter PickerView
    self.holeDiameterPickerView = [[UIPickerView alloc] init];
    self.holeDiameterPickerView.delegate = self;
    self.holeDiameterPickerView.dataSource = self;
    
    // JoistSize PickerView
    self.jointSizePickerView = [[UIPickerView alloc] init];
    self.jointSizePickerView.delegate = self;
    self.jointSizePickerView.dataSource = self;
    
    // DeadLoad PickerView
    self.deadLoadPickerView = [[UIPickerView alloc] init];
    self.deadLoadPickerView.delegate = self;
    self.deadLoadPickerView.dataSource = self;
}

- (void)doneButtonPressed {
    [self.view endEditing:YES];
}

- (void)addDataToPickerViewArrays {

    self.joistSpacingArray = @[@"300 to 600"];
    
    self.holeShapeArray = @[@"Circular",
                            @"Rectangular"];
    
    self.holeDiameterArray = @[@"75",
                               @"100",
                               @"125",
                               @"150",
                               @"175",
                               @"200",
                               @"225",
                               @"250"];
    
    self.jointSizeArray = @[@"SJ20044",
                            @"SJ24040",
                            @"SJ24051",
                            @"SJ24070",
                            @"SJ24090",
                            @"SJ30040",
                            @"SJ30051",
                            @"SJ30070",
                            @"SJ30090",
                            @"SJ36058",
                            @"SJ36090",
                            @"SJ40090"];
    
    self.deadLoadArray =  @[@"40kg/m2, 1.5kPa",
                            @"62kg/m2, 2.0kPa",
                            @"100kg/m2, 2.0kPa"];


}

- (void)calculate {
    // This Method will filter through a .plist database creating an array at the end depending on user inputs.
    
    // Load data to filter
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"webHole" ofType:@"plist"];
    NSArray *contentArray = [NSArray arrayWithContentsOfFile:plistPath];
    
    
    
}

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"modelSegue"]) {
        ModelWebViewController *modelView = [segue destinationViewController];
        [modelView setImageName:SINGLE_SPAN_IMAGE_NAME];
    }
}


@end
