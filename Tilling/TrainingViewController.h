//
//  TrainingViewController.h
//  Tilling
//
//  Created by Maris on 7/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ViewController.h"

@interface TrainingViewController : ViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
- (IBAction)contact:(id)sender;

@property (weak, nonatomic) IBOutlet UITextView *textView;


@end
