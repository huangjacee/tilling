//
//  ArchitecturalProductsTableViewController.h
//  Tilling
//
//  Created by Sharp Agency on 28/11/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArchitecturalProductsTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
