//
//  TextFieldWithNoCaret.m
//  Tilling
//
//  Created by Maris on 22/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "TextFieldWithNoCaret.h"

@implementation TextFieldWithNoCaret

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (CGRect)caretRectForPosition:(UITextPosition *)position {
    return CGRectZero;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
