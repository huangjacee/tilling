//
//  LintelViewController.m
//  Tilling
//
//  Created by Maris on 15/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "LintelViewController.h"
#import "ModelWebViewController.h"
#import "LintelCalcViewController.h"

#define SINGLE_SPAN_IMAGE_NAME @"lintel_single"

@interface LintelViewController () {
    NSString *modelImageName;
    NSArray *results;
}
@end

@implementation LintelViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Below I have created separate methods to handle object creation
    // in order to keep ViewDidLoad looking clean.
    // This class also contains methods linked to IB for textField change events

    [self addDataToPickerViewArrays];
    [self createPickerViews];
    
    // After above init, set input for textfield to pickerviews
    [self.spanConditionTextField setInputView:_spanConditionPickerView];
    [self.deadLoadTextField setInputView:_deadLoadPickerView];
    [self.loadWidthTextField setInputView:_loadWidthPickerView];
    [self.rafterSpacingTextField setInputView:_rafterSpacingPickerView];
    [self.windSpeedTextField setInputView:_windSpeedPickerView];
    [self.productTextField setInputView:_productPickerView];
    
    // Set accessoryView
    [self.spanTextField setInputAccessoryView:_accessoryView];
    [self.spanConditionTextField setInputAccessoryView:_accessoryView];
    [self.deadLoadTextField setInputAccessoryView:_accessoryView];
    [self.rafterSpacingTextField setInputAccessoryView:_accessoryView];
    [self.windSpeedTextField setInputAccessoryView:_accessoryView];
    [self.productTextField setInputAccessoryView:_accessoryView];
    
    // Set string defaults
    self.spanInt = 0;
    self.spanConditionString = @"Single Span";
    self.deadLoadString = @"40";
    self.loadWidthString = @"1500";
    self.rafterSpacingString = @"450";
    self.windSpeedString = @"N1";
    self.productString = @"SmartLVL15";
    
    modelImageName = SINGLE_SPAN_IMAGE_NAME;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 9;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Check and activate appropriate textfields depending on cell selected
    if (indexPath.row == 0) [self.spanTextField becomeFirstResponder];
    if (indexPath.row == 1) [self.spanConditionTextField becomeFirstResponder];
    if (indexPath.row == 2) [self.deadLoadTextField becomeFirstResponder];
    if (indexPath.row == 3) [self.loadWidthTextField becomeFirstResponder];
    if (indexPath.row == 4) [self.rafterSpacingTextField becomeFirstResponder];
    if (indexPath.row == 5) [self.windSpeedTextField becomeFirstResponder];
    if (indexPath.row == 6) [self.productTextField becomeFirstResponder];
    if (indexPath.row == 7) [self calculate];
    if (indexPath.row == 8) {
        if ([self.spanConditionString isEqualToString:@"Single Span"]) {
            modelImageName = SINGLE_SPAN_IMAGE_NAME;
            [self performSegueWithIdentifier:@"modelSegue" sender:self];
        }
    }
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    // remove all first responders
    [self.view endEditing:YES];
}

#pragma mark - UIPickerView Data Source
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == _spanConditionPickerView) return [_spanConditionArray count];
    if (pickerView == _deadLoadPickerView) return [_deadLoadArray count];
    if (pickerView == _loadWidthPickerView) return [_loadWidthArray count];
    if (pickerView == _rafterSpacingPickerView) return [_rafterSpacingArray count];
    if (pickerView == _windSpeedPickerView) return [_windSpeedArray count];
    if (pickerView == _productPickerView) return [_productArray count];

    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView == _spanConditionPickerView) return [_spanConditionArray objectAtIndex:row];
    if (pickerView == _deadLoadPickerView) return [_deadLoadArray objectAtIndex:row];
    if (pickerView == _loadWidthPickerView) return [_loadWidthArray objectAtIndex:row];
    if (pickerView == _rafterSpacingPickerView) return [_rafterSpacingArray objectAtIndex:row];
    if (pickerView == _windSpeedPickerView) return [_windSpeedArray objectAtIndex:row];
    if (pickerView == _productPickerView) return [_productArray objectAtIndex:row];
    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    // set textviews based on selection made in pickerView
    if (pickerView == _spanConditionPickerView) {
        self.spanConditionTextField.text = [_spanConditionArray objectAtIndex:row];
        self.spanConditionString = [_spanConditionArray objectAtIndex:row];
    }
    if (pickerView == _deadLoadPickerView) {
        self.deadLoadTextField.text = [NSString stringWithFormat:@"%@kg/m2", [_deadLoadArray objectAtIndex:row]];
        self.deadLoadString = [_deadLoadArray objectAtIndex:row];
    }
    if (pickerView == _loadWidthPickerView) {
        self.loadWidthTextField.text = [NSString stringWithFormat:@"%@mm", [_loadWidthArray objectAtIndex:row]];
        self.loadWidthString = [_loadWidthArray objectAtIndex:row];
    }
    if (pickerView == _rafterSpacingPickerView) {
        self.rafterSpacingTextField.text = [NSString stringWithFormat:@"%@mm", [_rafterSpacingArray objectAtIndex:row]];
        self.rafterSpacingString = [_rafterSpacingArray objectAtIndex:row];
    }
    if (pickerView == _windSpeedPickerView) {
        self.windSpeedTextField.text =  [_windSpeedArray objectAtIndex:row];
        self.windSpeedString = [_windSpeedArray objectAtIndex:row];
    }
    if (pickerView == _productPickerView) {
        self.productTextField.text = [_productArray objectAtIndex:row];
        self.productString = [_productArray objectAtIndex:row];
    }
}

#pragma mark - IB methods for textField changes
- (IBAction)spanEditingDidChange:(id)sender {
    self.spanInt = [_spanTextField.text intValue];
}

#pragma mark - init methods
- (void)createPickerViews {
    // Separate pickerviews for each occasion
    // makes it a little easier to make smaller custom changes and helps with debugging
    
    // Set up accessoryView. Used to dismiss pickerViews
    self.accessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
    [self.accessoryView setItems:@[_doneButton]];
    
    // SpanCondition PickerView
    self.spanConditionPickerView = [[UIPickerView alloc] init];
    self.spanConditionPickerView.delegate = self;
    self.spanConditionPickerView.dataSource = self;
    
    // DeadLoad PickerView
    self.deadLoadPickerView = [[UIPickerView alloc] init];
    self.deadLoadPickerView.delegate = self;
    self.deadLoadPickerView.dataSource = self;
    
    // LoadWidth PickerView
    self.loadWidthPickerView = [[UIPickerView alloc] init];
    self.loadWidthPickerView.delegate = self;
    self.loadWidthPickerView.dataSource = self;
    
    // RafterSpacing PickerView
    self.rafterSpacingPickerView = [[UIPickerView alloc] init];
    self.rafterSpacingPickerView.delegate = self;
    self.rafterSpacingPickerView.dataSource = self;
    
    // WindSpeed PickerView
    self.windSpeedPickerView = [[UIPickerView alloc] init];
    self.windSpeedPickerView.delegate = self;
    self.windSpeedPickerView.dataSource = self;
    
    // Product PickerView
    self.productPickerView = [[UIPickerView alloc] init];
    self.productPickerView.delegate = self;
    self.productPickerView.dataSource = self;
}

- (void)doneButtonPressed {
    [self.view endEditing:YES];
}

- (void)addDataToPickerViewArrays {
    self.spanConditionArray = @[@"Single Span"];
    
    self.deadLoadArray = @[@"40",
                           @"90"];
    
    self.loadWidthArray = @[@"1500",
                            @"2000",
                            @"4500",
                            @"6000",
                            @"7500"];
    
    self.rafterSpacingArray = @[@"450",
                                @"600",
                                @"900",
                                @"1200"];
    
    self.windSpeedArray = @[@"N1",
                            @"N2",
                            @"N3",
                            @"N4",
                            @"C1",
                            @"C2",
                            @"C3"];
    
    self.productArray = @[@"SmartLVL15",
                          @"SmartLam GL13c",
                          @"SmartLam GL17c",
                          @"SmartLam GL18c"];
}

#pragma mark - Perform Calculation
- (void)calculate {
    // This Method will filter through a .plist database creating an array at the end depending on user inputs.
    
    // Load data to filter
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"rafterLintelData" ofType:@"plist"];
    NSArray *contentArray = [NSArray arrayWithContentsOfFile:plistPath];
    
    // Set windspeed to integer to match with the datasource values. Aparently these are in 2 groups.
    NSString *windSpeedNumber = @"0";
    if ([_windSpeedString isEqualToString:@"N1"] ||
        [_windSpeedString isEqualToString:@"N2"] ||
        [_windSpeedString isEqualToString:@"N3"] ||
        [_windSpeedString isEqualToString:@"N4"]) windSpeedNumber = @"1";
    
    else if ([_windSpeedString isEqualToString:@"C1"] ||
             [_windSpeedString isEqualToString:@"C2"] ||
             [_windSpeedString isEqualToString:@"C3"]) windSpeedNumber = @"2";
    
    // set product id to integer to match with datasource value
    NSString *productIDNumber = @"0";
    if ([_productString isEqualToString:@"SmartLVL15"]) productIDNumber = @"1";
    else if ([_productString isEqualToString:@"SmartLam GL13c"]) productIDNumber = @"2";
    else if ([_productString isEqualToString:@"SmartLam GL17c"]) productIDNumber = @"3";
    else if ([_productString isEqualToString:@"SmartLam GL18c"]) productIDNumber = @"4";

    // 1 DeadLoad Filter
    NSPredicate *deadLoadPredicate = [NSPredicate predicateWithFormat:@"SELF.roofDL CONTAINS %@", _deadLoadString];
    NSArray *filteredByDeadLoad = [NSArray arrayWithArray:[contentArray filteredArrayUsingPredicate:deadLoadPredicate]];
    
    // 2 LoadWidth Filter
    NSPredicate *loadWidthPredicate = [NSPredicate predicateWithFormat:@"SELF.roof_load_width CONTAINS %@", _loadWidthString];
    NSArray *filteredByLoadWidth = [NSArray arrayWithArray:[filteredByDeadLoad filteredArrayUsingPredicate:loadWidthPredicate]];
    
    // 3 RafterSpacing Filter
    NSPredicate *rafterPredicate = [NSPredicate predicateWithFormat:@"SELF.rafter_spacing CONTAINS %@", _rafterSpacingString];
    NSArray *filteredByRafter = [NSArray arrayWithArray:[filteredByLoadWidth filteredArrayUsingPredicate:rafterPredicate]];
    
    // 4 WindSpeed Filter
    NSPredicate *windSpeedPredicate = [NSPredicate predicateWithFormat:@"SELF.windSpeedGroup CONTAINS %@", windSpeedNumber];
    NSArray *filteredByWind = [NSArray arrayWithArray:[filteredByRafter filteredArrayUsingPredicate:windSpeedPredicate]];
    
    // 5 Product Filter
    NSPredicate *productPredicate = [NSPredicate predicateWithFormat:@"SELF.productID CONTAINS %@", productIDNumber];
    NSArray *filteredByProduct = [NSArray arrayWithArray:[filteredByWind filteredArrayUsingPredicate:productPredicate]];
    
    // 6 Span Filter
    NSPredicate *spanPredicate = [NSPredicate predicateWithFormat:@"SELF.span.intValue >= %d", _spanInt];
    NSArray *filteredBySpan = [NSArray arrayWithArray:[filteredByProduct filteredArrayUsingPredicate:spanPredicate]];
    
    results = filteredBySpan;
    
    NSLog(@"Raw Count: %lu", (unsigned long)[contentArray count]);
    NSLog(@"DeadLoad Count: %lu", (unsigned long)[filteredByDeadLoad count]);
    NSLog(@"LoadWidth Count: %lu", (unsigned long)[filteredByLoadWidth count]);
    NSLog(@"Rafter Count: %lu", (unsigned long)[filteredByRafter count]);
    NSLog(@"Wind Count: %lu", (unsigned long)[filteredByWind count]);
    NSLog(@"Product Count: %lu", (unsigned long)[filteredByProduct count]);
    NSLog(@"Span Count: %lu", (unsigned long)[filteredBySpan count]);
    
    [self performSegueWithIdentifier:@"resultsSegue" sender:self];
}

#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"modelSegue"]) {
        ModelWebViewController *modelView = [segue destinationViewController];
        [modelView setImageName:modelImageName];
    }
    if ([[segue identifier] isEqualToString:@"resultsSegue"]) {
        LintelCalcViewController *vc = [segue destinationViewController];
        [vc setContent:results];
    }
}


@end
