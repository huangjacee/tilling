//
//  TextFieldWithNoCaret.h
//  Tilling
//
//  Created by Maris on 22/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldWithNoCaret : UITextField

@end
