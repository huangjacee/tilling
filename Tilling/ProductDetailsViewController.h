//
//  ProductDetailsViewController.h
//  Tilling
//
//  Created by Maris on 10/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScrollImageView.h"

@interface ProductDetailsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet ScrollImageView *scrollingImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *arrayOfData;
@property (strong, nonatomic) NSArray *arrayOfTitles;
@property (strong, nonatomic) NSArray *colorArray;

@property (strong, nonatomic) NSMutableDictionary *data;
@property (strong, nonatomic) NSString *imagePath;


@end
