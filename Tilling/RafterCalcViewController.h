//
//  RafterCalcViewController.h
//  Tilling
//
//  Created by Maris on 7/11/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RafterCalcViewController : UITableViewController

@property (strong, nonatomic) NSArray *content;

@end
