//
//  ProductTableviewViewController.h
//  Tilling
//
//  Created by Maris on 9/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductTableviewViewController : UITableViewController

@property (nonatomic, strong) NSArray *images;
@property (nonatomic, strong) NSArray *titles;

@property (nonatomic, strong) NSArray *products;


@end
