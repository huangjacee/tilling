//
//  LintelCalcViewController.m
//  Tilling
//
//  Created by Maris on 7/11/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "LintelCalcViewController.h"
#import "CustomCalculatorCell.h"

@interface LintelCalcViewController ()

@end

@implementation LintelCalcViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if ([_content count]) return [_content count];
    else return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Set Cell identifier
    static NSString *CellIdentifier = @"Cell";
    
    // if content exists, display it................
    if ([_content count]) {
        CustomCalculatorCell *cell = (CustomCalculatorCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        // If no cell, create one with custom nib
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomCalculatorCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        // Configure the cell...
        cell.sizeLabel.text = [NSString stringWithFormat:@"%@", [[_content objectAtIndex:indexPath.row] valueForKey:@"DxB"]];
        cell.spanLabel.text = [[_content objectAtIndex:indexPath.row] valueForKey:@"span"];
        cell.ebLabel.text = [[_content objectAtIndex:indexPath.row] valueForKey:@"EB"];
        
        return cell;
    }
    // if content doesnt exist, inform the user.................
    else {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.textLabel.text = @"No results within bounds of calculation";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
        
        tableView.separatorColor = [UIColor clearColor];
        
        return cell;
    }

}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ([_content count]) return @"      Size                   Span                          EB"; // show header if there are results to display
    else return nil; // no results, no header
}

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
