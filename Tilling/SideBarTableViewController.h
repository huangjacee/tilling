//
//  SideBarTableViewController.h
//  Tilling
//
//  Created by Maris on 7/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideBarTableViewController : UITableViewController

@property (strong, nonatomic) NSArray *menuItems;


@end
