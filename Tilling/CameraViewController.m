//
//  CameraViewController.m
//  Tilling
//
//  Created by Beau Young on 5/12/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "CameraViewController.h"

@interface CameraViewController ()

@property (strong, nonatomic) UIImageView *texture;

@end

@implementation CameraViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

}

- (IBAction)openCamera:(UIBarButtonItem *)sender {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [imagePickerController setDelegate:self];
        [imagePickerController setAllowsEditing:NO];
        [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        
        // Place image picker on the screen
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }
}

- (IBAction)chooseFromLibrary:(UIBarButtonItem *)sender {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        [imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        
        // image picker needs a delegate so we can respond to its messages
        [imagePickerController setDelegate:self];
        
        // Place image picker on the screen
        [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (IBAction)removeImage:(UIBarButtonItem *)sender {
    self.imageView.image = nil;
}



- (IBAction)addTexture:(UIBarButtonItem *)sender {
    self.texture.image = [UIImage imageNamed:@"tilling_back"];
    [self.view addSubview:self.texture];
    
}


- (IBAction)panGestureChanged:(UIPanGestureRecognizer *)sender {

}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    self.imageView.image = image;

    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
