//
//  ScrollImageView.h
//  Tilling
//
//  Created by Beau Young on 16/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScrollImageView : UIImageView {
    
    int _currentFrame, _totalFrames, startFrame;
    CGPoint startPt;
    
    
    
}

@property (nonatomic, assign) int currentFrame, totalFrames;

-(id)initWithFrame:(CGRect)frame andImagePath:(NSString *)imagePath;


@end
