//
//  SmartFrameViewController.m
//  Tilling
//
//  Created by Maris on 7/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "SmartFrameViewController.h"
#import "ArchitecturalTableViewCell.h"

@implementation SmartFrameViewController {
    NSIndexPath *selectedCellIndexPath;
    NSArray *cellBackgroundImageArray;
    NSArray *cellTitles;
    NSArray *cellDetailText;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.146 green:0.682 blue:0.759 alpha:0.800];

    cellTitles = @[@"SmartFrame",
                   @"Products",
                   @"Design Software",
                   @"Design Centre",
                   @"Product Profiles",
                   @"Calculators"];
    
    cellBackgroundImageArray = @[[UIImage imageNamed:@"smart_1"],
                                 [UIImage imageNamed:@"smart_2"],
                                 [UIImage imageNamed:@"smart_3"],
                                 [UIImage imageNamed:@"smart_4"],
                                 [UIImage imageNamed:@"smart_6"],
                                 [UIImage imageNamed:@"smart_7"]];
    
    cellDetailText = @[@"SmartFrame is a full range of Laminated Veneer Lumber(LVL), SmartJoists and SmartLam beams. Backed up by a state of the art Design Compendium that includes the SmartFrame software package as well as the convenience of a full technical library which includes all the SmartFrame Engineered Timber.\n\nThe design team is made up of Engineers, Building Designers, Builders, Building Surveyors and Frame & Truss Detailers all working together to provide an integrated solution for your project requirements.",
                       
                       @"Insert text here",
                       @"Insert text here",
                       
                       @"The design team is made up of Engineers, Building Designers, Builders, Building Surveyors and Frame & Truss Detailers all working together to provide an integrated solution for your project requirements.\n\nWhat do we do?\n\nThe following services are provided by the SmartFrame Design Centre.\n\n1. Full engineering support on all SmartFrame Engineered Wood Products\n\n2. Free design and take off from building plans\n\n3. Dedicated toll free engineering support line/n/n4. Structured SmartFrame software training program (Approved Vic building comminssion CPD provider)\n\n5. Certification of engineering designs to relevant state Building acts and regulations./n/nSmartframe Design CentreEngineering Support\n\nOur dedicated team of designers are able to provide high quality material picking lists which add our customers within the estimating, sales and order process.\n\n\nSmartframe Design CentreDesign & Take-off Services\n\nTilling provide indirect customers the opportunity to submit plans for a proposed construction through their local Timber Merchant and Tilling shall log the project and provide a full quotation and set of plans back to the nominated Merchant.\n\nIn order to facilitate a speedy return of your request for quote application, there are a number of things that you can prepare to support the process. These include:\n\n1. The more data the better, including the Architectural drawings with full architectural details, including dimension and elevations sections etc..\n\n2. Engineering details are also required in order to ensure we cover all critical structural elements of the project.\n\n3. All files in a .pdf or AutoCad/DXF/DWG format.\n\nIf in doubt as to what you should or shouldn't include, please call (03) 9726 2198 and speak to our administration staff.\n\nSmartframe Design CentreTechnical Support\n\nWe have a dedicated Engineering Technical Support Services person ready and able to provide you with accurate, timely and professional advice regarding your construction project.\n\nThis support service is available 6 days a week, Monday to Saturday on 1300 668 690.",
                       
                       @"",
                       @""];
    
}
#pragma mark - TableView delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // set cell identifier
    static NSString *CellIdentifier = @"Cell";
    
    // implement custom uitableview cell
    ArchitecturalTableViewCell *cell = (ArchitecturalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArchitecturalTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    // Configure the cells...
    cell.backgroundImage.image = [cellBackgroundImageArray objectAtIndex:indexPath.section];    
    cell.titleLabel.text = [cellTitles objectAtIndex:indexPath.section];
    cell.detailLabel.text = [cellDetailText objectAtIndex:indexPath.section];
    cell.contentView.backgroundColor = [UIColor colorWithRed:0.081 green:0.287 blue:0.483 alpha:1.000];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView beginUpdates]; // tell the table you're about to start making changes
    
    // If the index path of the currently expanded cell is the same as the index that
    // has just been tapped set the expanded index to nil so that there aren't any
    // expanded cells, otherwise, set the expanded index to the index that has just
    // been selected.
    
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) selectedCellIndexPath = nil;
    else selectedCellIndexPath = indexPath;
    
    [tableView endUpdates]; // tell the table you're done making your changes
    
    if (indexPath.section == 5) [self performSegueWithIdentifier:@"calculatorsSegue" sender:nil];
    if (indexPath.section == 1) [self performSegueWithIdentifier:@"productsSegue" sender:nil];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Compares the index path for the current cell to the index path stored in the expanded
    // index path variable. If the two match, return a height of 100 points, otherwise return
    // a height of 44 points.
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) {
        if (indexPath.section == 0) return 300; // Smart Frame
        if (indexPath.section == 2) return 550; // design software
        if (indexPath.section == 3) return 1300; // design centre
    }
    return 56;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 4) {
        return 56;
    }
    return 0; // you can have your own choice, of course
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
