//
//  ViewController.m
//  Tilling
//
//  Created by Maris on 2/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ViewController.h"
#import "SWRevealViewController.h"
#import "ArchitecturalTableViewCell.h"
#import "TillingBuildAHomeViewController.h"
#import "CameraViewController.h"

@interface ViewController () {
    NSIndexPath *selectedCellIndexPath;
    NSArray *backgroundImageArray;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    // Set datasource and delegates for tableview
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithWhite:0.463 alpha:0.800];
    
    // set up image array
    backgroundImageArray = @[[UIImage imageNamed:@"tilling_1"],
                             [UIImage imageNamed:@"tilling_2"],
                             [UIImage imageNamed:@"tilling_6"],
                             [UIImage imageNamed:@"tilling_7"]];
    
    // set the side bar button action
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    // set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    // set shadow thrown on rear view controller
    self.revealViewController.frontViewShadowOffset = CGSizeMake(1, 0);
    self.revealViewController.frontViewShadowRadius = 20;
    self.revealViewController.frontViewShadowOpacity = .3;
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
    if (![[defaults stringForKey:@"keyDisclaimer"] isEqualToString:@"accepted"]) {
        UIAlertView *disclaimer = [[UIAlertView alloc] initWithTitle:@"Disclaimer" message:@"By selecting the 'Accept' button you agree to the Full License Agreement on our website. If you do not accept these terms and conditions please uninstall and delete the application." delegate:self cancelButtonTitle:@"Accept" otherButtonTitles:nil];
        [disclaimer show];
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // set cell identifier
    static NSString *CellIdentifier = @"Cell";
    
    // implement custom uitableviewcell
    ArchitecturalTableViewCell *cell = (ArchitecturalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArchitecturalTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    // Arrays containing content for cells - add to these and add more rows when needing more content
    NSArray *titleStrings = @[@"About Us", @"Chain of Custody", @"Build a Home", @"Texture Mapper"];
    NSArray *detailStrings = @[
                               @"Tilling Timber is one of Australia’s largest timber wholesaler/importers. A privately held company, Tilling Timber Pty Ltd was established by the present owners in November 1963 where they started their first factory in Eltham Victoria. \n\n The company now has large, modern wholesale sales offices and warehouses in Brisbane, Sydney, Melbourne and Perth, as well as manufacturing facilities at its head office in Kilsyth, Victoria. Today the Tilling Timber Group of companies comprising of Tilling Timber Pty Ltd., Tilling Timber (Sydney) and Tilling Timber Imports Pty Ltd. has 212 employees and is 100% Australian owned. \n\n The product range includes solid softwood structural timbers, solid timber lining & cladding profiles, shingles, shakes, finished mouldings and the latest addition to our comprehensive range - LUSTRE. Lustre ceiling and lining boards are pre-finished with four coats of polyurethane in a satin finish. So once installed, the job is done. No smells, no mess - it's that simple. \n\n The company also carries its own unique range of Engineered Wood Products known as SmartFrame. Products in this range include SmartLam glued laminated beams, Smart LVL, SmartJoist I-joists plus the state of the art software program and technical library in the SmartFrame Design Compendium. Tilling Timber is Australia’s foremost solid timber profile woods producer. The manufacturing facility, situated on a 20 acre site located at Kilsyth Victoria, is equipped with the latest ‘state of the art’ plant machinery. \n\n Equipment includes dehumidifying drying kilns with a combined charge capacity of 370 cube metres, hydromat Weinig moulders with a combined capacity on a one shift basis of 65,000 lineal metres, and a unique multi-million dollar continuous pre-finishing line with a capacity of 30,000 lineal metres on a two shift basis. A fully automated SmartFrame cutting system which provides pre-cut penetrations in SmartJoists has recently been installed. The company distributes its products on a wholesale basis only, through its national network of sales offices and distribution warehouses. \n\n The SmartFrame Design Centre team is made up of Engineers, Building Designers, Builders, Building Surveyors and F&T Detailers all working together to provide an integrated solution for your project requirements. Services provided by the SmartFrame Design Centre are: - Full engineering support on all SmartFrame EWP - Free design and take off from building plans - Dedicated toll free engineering support line - Structured SmartFrame software training program (Approved Vic building commission CPD provider) - Certification of engineering designs to relevant state Building acts and regulations. \n\n Tilling Timber Vic, located on a 20 acre site in the Melbourne suburb of Kilsyth, incorporates the manufacturing complex for the Tilling Group of companies, as well as sales and extensive warehousing and distribution facilities for Victoria, Tasmania and South Australia. The administrative head office for the Tilling Timber Group and the SmartFrame Design Centre are also located on this site.",
                               
                               @"Chain of Custody (CoC) tracks the path taken by a forest product from its origin in a certified forest, right through to its end use by the consumer. \n\n It includes every link in the supply chain - such as harvesting, transportation, primary and secondary processing, manufacturing, re-manufacturing, distribution and sales. It means that when buying forest products, customers can select certified products that have a traceable source.",
                               
                               @"",
                               @""];
    
    // set the label in Nib
    cell.titleLabel.text = [titleStrings objectAtIndex:indexPath.section];
    cell.detailLabel.text = [detailStrings objectAtIndex:indexPath.section];
    cell.backgroundImage.image = [backgroundImageArray objectAtIndex:indexPath.section];
    cell.contentView.backgroundColor = [UIColor colorWithRed:0.411 green:0.425 blue:0.488 alpha:1.000];
    
    return cell;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"accepted"forKey:@"keyDisclaimer"];
    [defaults synchronize];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView beginUpdates]; // tell the table you're about to start making changes
    
    // If the index path of the currently expanded cell is the same as the index that
    // has just been tapped set the expanded index to nil so that there aren't any
    // expanded cells, otherwise, set the expanded index to the index that has just
    // been selected.
    
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) selectedCellIndexPath = nil;
    else selectedCellIndexPath = indexPath;
    
    [tableView endUpdates]; // tell the table you're done making your changes
    
    if (indexPath.section == 2) {
        [self performSegueWithIdentifier:@"BuildAHomeSegue" sender:nil];
    }
    if (indexPath.section == 3) {
        [self performSegueWithIdentifier:@"textureSegue" sender:nil];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Compares the index path for the current cell to the index path stored in the expanded
    // index path variable. If the two match, return a height of 100 points, otherwise return
    // a height of 44 points.
    
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) {
        if (indexPath.section == 0) return 1320.0; // Expanded height
        else if (indexPath.section == 1) return 270;
    }
    return 56; // Normal height
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        return 168;
    }
    return 0; // you can have your own choice, of course
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"BuildAHomeSegue"])
    {
        TillingBuildAHomeViewController *buildAHomeController = [segue destinationViewController];
        buildAHomeController.tableView = nil;
        
    }
    if ([[segue identifier] isEqualToString:@"textureSegue"]) {
        CameraViewController *camera = [segue destinationViewController];
    }
}
@end
