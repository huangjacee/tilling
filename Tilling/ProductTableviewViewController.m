//
//  ProductTableviewViewController.m
//  Tilling
//
//  Created by Maris on 9/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ProductTableviewViewController.h"
#import "ProductTableViewCell.h"
#import "ProductDetailsViewController.h"

@interface ProductTableviewViewController () 

@end

@implementation ProductTableviewViewController {
    NSIndexPath *savedIndexpath;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Load the list of products so we can populate the tableView
    self.products = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ProductList" ofType:@"plist"]];
    
    // Removes the tableview separator for a cleaner Look.
    self.tableView.separatorColor = [UIColor clearColor];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.products count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Set Cell identifier
    static NSString *CellIdentifier = @"Cell";
    ProductTableViewCell *cell = (ProductTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // If no cell, create one with custom nib
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductListTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    // Configure the cell...
    cell.customTitleLabel.text = [[self.products objectAtIndex:indexPath.row] valueForKey:@"title"];
    UIImage *image = [UIImage imageNamed:[[self.products objectAtIndex:indexPath.row] valueForKey:@"image"]];
    
    // This sets the image and stops the app from crashing if no image can be found.
    if (image) {
        cell.customImageView.image = image;
    }
    
    // create background color for selected state of cell.
    UIView *cellSelectedBackground = [[UIView alloc] init];
    cellSelectedBackground.backgroundColor = [UIColor colorWithRed:0.987 green:0.558 blue:0.178 alpha:1.000];
    cell.selectedBackgroundView = cellSelectedBackground;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    savedIndexpath = [[NSIndexPath alloc] init];
    savedIndexpath = indexPath;
    
    [self performSegueWithIdentifier:@"ProductDetailsSegue" sender:nil];
    
}

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    ProductDetailsViewController *vc = [segue destinationViewController];
    [vc setData:[self.products objectAtIndex:savedIndexpath.row]];
}



@end
